﻿// <copyright file="ClaimDefinitions.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Product.V1
{
    using ITUtil.Common.Command;
    using System.Collections.Generic;

    /// <summary>
    /// ClaimDefinitions
    /// </summary>
    public static class ClaimDefinitions
    {
        /// <summary>
        /// The admin claim, which gives the user the rights for maintaining security
        /// </summary>
        internal static readonly Claim IsAdmin = new Claim()
        {
            key = "Monosoft.Service.Product.isAdmin",
            dataContext = Claim.DataContextEnum.organisationClaims,
            description = new ITUtil.Common.Base.LocalizedString[] { new ITUtil.Common.Base.LocalizedString("en", "User is administrator, i.e. can see all orders") }
        };
    }
}
