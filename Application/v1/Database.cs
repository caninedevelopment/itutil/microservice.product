﻿// <copyright file="Database.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Product.V1
{
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.Linq;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;
    using Newtonsoft.Json.Linq;
    using ServiceStack.OrmLite;

    /// <summary>
    /// Database
    /// </summary>
    public class Database
    {
        private static OrmLiteConnectionFactory dbFactory = null;
        private static string dbName = "product";

        /// <summary>
        /// Initializes a new instance of the <see cref="Database"/> class.
        /// </summary>
        public Database(string settingsFilename = null)
        {
            var settings = MicroServiceConfig.getConfig(settingsFilename);

            OrmLiteConnectionFactory masterDb = null;
            masterDb = GetConnectionFactory(settings.SQL);

            using (var db = masterDb.Open())
            {
                settings.SQL.CreateDatabaseIfNoExists(db, dbName);
            }

            dbFactory = GetConnectionFactory(settings.SQL, dbName);
            using (var db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<Product.V1.DTO.Product>();
            }
        }

        private static OrmLiteConnectionFactory GetConnectionFactory(SQLSettings settings, string databasename = "")
        {
            switch (settings.ServerType)
            {
                case SQLSettings.SQLType.MSSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), SqlServer2016Dialect.Provider);
                case SQLSettings.SQLType.POSTGRESQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), PostgreSqlDialect.Provider);
                case SQLSettings.SQLType.MYSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), MySqlDialect.Provider);
                default:
                    return null;
            }
        }

        /// <summary>
        /// CreateProduct
        /// </summary>
        /// <param name="input">Service.Product.V1.DTO.Product input</param>
        /// <returns>bool</returns>
        public void CreateProduct(Service.Product.V1.DTO.Product input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Service.Product.V1.DTO.Product>(input.id);

                if (dbresult == null)
                {
                    db.Insert<Service.Product.V1.DTO.Product>(input);
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementAlreadyExistsException("Product already exist", input.id.ToString());
                }
            }
        }

        /// <summary>
        /// UpdateProduct
        /// </summary>
        /// <param name="input">DTO.Product input</param>
        /// <returns>bool</returns>
        public void UpdateProduct(Service.Product.V1.DTO.Product input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Service.Product.V1.DTO.Product>(input.id);

                if (dbresult != null)
                {
                    db.Update<Service.Product.V1.DTO.Product>(input);
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistsException("Product does not exist", input.id.ToString());
                }
            }
        }

        /// <summary>
        /// ReadProduct
        /// </summary>
        /// <param name="guidId">ITUtil.Common.DTO.GuidIdDTO guidId</param>
        /// <returns>Service.Product.V1.DTO.Product</returns>
        public Service.Product.V1.DTO.Product ReadProduct(DTO.GuidIdDTO guidId)
        {
            if (guidId == null)
            {
                throw new ArgumentNullException(nameof(guidId));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<Service.Product.V1.DTO.Product>(guidId.id);
                if (dbresult != null)
                {
                    return dbresult;
                } else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistsException("Product does not exist", guidId.id.ToString());
                }
            }
        }


        private void removeFields(JToken token, string[] fields, int depth = 0)
        {
            JContainer container = token as JContainer;
            if (container == null) return;

            List<JToken> removeList = new List<JToken>();
            foreach (JToken el in container.Children())
            {
                JProperty p = el as JProperty;
                if (p != null && fields.Length-1 == depth/2 && fields[depth / 2] == p.Name)
                {
                    removeList.Add(el);
                }
                removeFields(el, fields, depth+1);
            }

            foreach (JToken el in removeList)
            {
                el.Remove();
            }
        }

        /// <summary>
        /// CreateCategory
        /// </summary>
        /// <param name="excludepproperty">excludepproperty</param>
        /// <returns>List<Service.Product.V1.DTO.Product></returns>
        public List<Service.Product.V1.DTO.Product> GetAll(Service.Product.V1.DTO.ExcludepProperty excludepproperty)
        {
            if (excludepproperty == null)
            {
                throw new ArgumentNullException(nameof(excludepproperty));
            }

            List<Service.Product.V1.DTO.Product> product = new List<Service.Product.V1.DTO.Product>();
            using (var db = dbFactory.Open())
            {
                var dbresult = db.Select<Service.Product.V1.DTO.Product>();
                if (dbresult != null)
                {
                    product = dbresult;
                    for (int i = 0; i < product.Count; i++)
                    {
                        var json = JObject.Parse(product[i].json);
                        foreach (var item in excludepproperty.excludeProperty)
                        {
                            var path = item.Split('.');
                            removeFields(json, path);
                         
                        }
                        product[i].json = json.ToString();
                    }
                }

                return product;
            }
        }
    }
}
