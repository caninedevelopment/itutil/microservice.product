﻿//// <copyright file="Controller.cs" company="Monosoft Holding ApS">
//// Copyright 2018 Monosoft Holding ApS
////
//// Licensed under the Apache License, Version 2.0 (the "License");
//// you may not use this file except in compliance with the License.
//// You may obtain a copy of the License at
////
////    http://www.apache.org/licenses/LICENSE-2.0
////
//// Unless required by applicable law or agreed to in writing, software
//// distributed under the License is distributed on an "AS IS" BASIS,
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//// See the License for the specific language governing permissions and
//// limitations under the License.
//// </copyright>

//namespace Monosoft.Service.Product.V1
//{
//    using System;
//    using System.Collections.Generic;
//    using ITUtil.Common.DTO;
//    using ITUtil.Common.Utils;
//    using ITUtil.Common.Utils.Bootstrapper;
//    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

//    /// <summary>
//    /// Controller
//    /// </summary>
//    public class Controller : IMicroservice
//    {
//        public static Database db = new Database();

//        /// <inheritdoc/>
//        public string ServiceName
//        {
//            get
//            {
//                return "Product";
//            }
//        }

//        /// <inheritdoc/>
//        public ProgramVersion ProgramVersion
//        {
//            get
//            {
//                return new ProgramVersion("1.0.0.0");
//            }
//        }

//        /// <summary>
//        /// Gets OperationDescription
//        /// </summary>
//        public List<OperationDescription> OperationDescription
//        {
//            get
//            {
//                return new List<OperationDescription>()
//                {
//                    new OperationDescription(
//                        "create",
//                        "Create a product",
//                        typeof(Service.Product.V1.DTO.Product),
//                        typeof(ITUtil.Common.DTO.BoolDTO),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { Service.Product.V1.ClaimDefinitions.IsAdmin }),
//                        CreateProduct),
//                    new OperationDescription(
//                        "update",
//                        "update a product",
//                        typeof(Service.Product.V1.DTO.Product),
//                        typeof(ITUtil.Common.DTO.BoolDTO),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { Service.Product.V1.ClaimDefinitions.IsAdmin }),
//                        UpdateProduct),
//                    new OperationDescription(
//                        "read",
//                        "read a product",
//                        typeof(ITUtil.Common.DTO.GuidIdDTO),
//                        typeof(Service.Product.V1.DTO.Product),
//                        null,
//                        ReadProduct),
//                    new OperationDescription(
//                        "getall",
//                        "get all products",
//                        typeof(Service.Product.V1.DTO.ExcludepProperty),
//                        typeof(List<Service.Product.V1.DTO.Product>),
//                        null,
//                        GetAll),
//                };
//            }
//        }

//        /// <summary>
//        /// CreateProduct
//        /// </summary>
//        /// <param name="wrapper">MessageWrapper wrapper</param>
//        /// <returns>ReturnMessageWrapper</returns>
//        public static ReturnMessageWrapper CreateProduct(MessageWrapper wrapper)
//        {
//            if (wrapper == null)
//            {
//                throw new ArgumentNullException(nameof(wrapper));
//            }

//            var input = MessageDataHelper.FromMessageData<Service.Product.V1.DTO.Product>(wrapper.messageData);
//            if (db.CreateProduct(input))
//            {
//                return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
//            }
//            else
//            {
//                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Product already exists"));
//            }
//        }

//        /// <summary>
//        /// UpdateProduct
//        /// </summary>
//        /// <param name="wrapper">MessageWrapper wrapper</param>
//        /// <returns>ReturnMessageWrapper</returns>
//        public static ReturnMessageWrapper UpdateProduct(MessageWrapper wrapper)
//        {
//            if (wrapper == null)
//            {
//                throw new ArgumentNullException(nameof(wrapper));
//            }

//            var input = MessageDataHelper.FromMessageData<Service.Product.V1.DTO.Product>(wrapper.messageData);
//            if (db.UpdateProduct(input))
//            {
//                return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
//            }
//            else
//            {
//                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Product not found"));
//            }
//        }

//        /// <summary>
//        /// ReadProduct
//        /// </summary>
//        /// <param name="wrapper">MessageWrapper wrapper</param>
//        /// <returns>ReturnMessageWrapper</returns>
//        public static ReturnMessageWrapper ReadProduct(MessageWrapper wrapper)
//        {
//            if (wrapper == null)
//            {
//                throw new ArgumentNullException(nameof(wrapper));
//            }

//            var input = MessageDataHelper.FromMessageData<ITUtil.Common.DTO.GuidIdDTO>(wrapper.messageData);
//            var data = db.ReadProduct(input);
//            if (data != null)
//            {
//                return ReturnMessageWrapper.CreateResult(true, wrapper, data, LocalizedString.OK);
//            }
//            else
//            {
//                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Product not found"));
//            }
//        }

//        /// <summary>
//        /// GetAll
//        /// </summary>
//        /// <param name="wrapper">MessageWrapper wrapper</param>
//        /// <returns>ReturnMessageWrapper</returns>
//        public static ReturnMessageWrapper GetAll(MessageWrapper wrapper)
//        {
//            if (wrapper == null)
//            {
//                throw new ArgumentNullException(nameof(wrapper));
//            }

//            var input = MessageDataHelper.FromMessageData<Service.Product.V1.DTO.ExcludepProperty>(wrapper.messageData);
//            var data = db.GetAll(input);
//            if (data != null)
//            {
//                return ReturnMessageWrapper.CreateResult(true, wrapper, data, LocalizedString.OK);
//            }
//            else
//            {
//                return ReturnMessageWrapper.CreateResult(false, wrapper, null, LocalizedString.Create("EN", "Product not found"));
//            }
//        }
//    }
//}
