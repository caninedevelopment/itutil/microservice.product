﻿CREATE TABLE Product (
	Id varchar PRIMARY KEY,
	[Data] varchar
);

CREATE TABLE Category (
	Id varchar PRIMARY KEY,
	[Data] varchar
);

CREATE TABLE Category_Product (
	Id varchar PRIMARY KEY,
	Product_Id varchar,
	Category_Id varchar
);
