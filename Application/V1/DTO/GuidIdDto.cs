﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.Product.V1.DTO
{
    public class GuidIdDTO
    {

        public Guid id { get; set; }
    }
}
