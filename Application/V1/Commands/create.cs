﻿namespace Monosoft.Service.Product.V1.Commands
{
    public class create : ITUtil.Common.Command.InsertCommand<DTO.Product>
    {
        public create()
            : base("Create a product")
        {
            this.Claims.Add(ClaimDefinitions.IsAdmin);
        }

        public void Execute(DTO.Product input)
        {
            Namespace.db.CreateProduct(input);
        }
    }
}