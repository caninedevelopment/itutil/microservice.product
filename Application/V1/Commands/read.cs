﻿namespace Monosoft.Service.Product.V1.Commands
{
    public class read : ITUtil.Common.Command.GetCommand<Service.Product.V1.DTO.GuidIdDTO, DTO.Product>
    {
        public read()
            : base("read a product")
        {
            //TODO: issue: der er ingen claims, i.e. alle kan hente digitale produkter ned i deres helhed
        }

        public DTO.Product Execute(Service.Product.V1.DTO.GuidIdDTO input)
        {
            return Namespace.db.ReadProduct(input);
        }
    }
}