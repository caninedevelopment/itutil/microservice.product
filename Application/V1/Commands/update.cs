﻿namespace Monosoft.Service.Product.V1.Commands
{
    public class update : ITUtil.Common.Command.UpdateCommand<DTO.Product>
    {
        public update()
            : base("update a product")
        {
            this.Claims.Add(ClaimDefinitions.IsAdmin);
        }

        public void Execute(DTO.Product input)
        {
            Namespace.db.UpdateProduct(input);
        }
    }
}
