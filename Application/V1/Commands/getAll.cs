﻿using System.Collections.Generic;

namespace Monosoft.Service.Product.V1.Commands
{

    public class getAll : ITUtil.Common.Command.GetCommand<Service.Product.V1.DTO.ExcludepProperty, List<Service.Product.V1.DTO.Product>>
    {
        public getAll()
            : base("get all products")
        {
            //TODO: issue: der er ingen claims, i.e. alle kan hente digitale produkter ned i deres helhed
        }

        public List<Service.Product.V1.DTO.Product> Execute(Service.Product.V1.DTO.ExcludepProperty input)
        {
            return Namespace.db.GetAll(input);
        }
    }
}
