﻿// <copyright file="UnitTest.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests
{
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;
    using Monosoft.Service.Product.V1;
    using ITUtil.Common.Utils;
    using Monosoft.Service.Product.V1.Commands;

    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        private string jsontext = "{\"name\": \"bog1\", \"variants\": [{\"name\": \"rabbit\",\"gender\": \"girl\",\"pris\": \"3000\",\"cover\": \"svg\", \"pages\": [ \"svg\", \"svg\"]},{\"name\": \"rabbit\",\"gender\": \"girl\",\"pris\": \"3000\",\"cover\": \"svg\", \"pages\": [ \"svg\", \"svg\"]}]}";

        /// <summary>
        /// CreateProduct
        /// </summary>
        [Test]
        public void CreateProduct()
        {

            foreach (var configfile in Common.configurations)
            {
                Namespace.db = new Database(configfile);
                create cmd = new create();
                cmd.Execute(new Monosoft.Service.Product.V1.DTO.Product()
                {
                    id = Guid.NewGuid(),
                    json = jsontext,
                    categories = new List<string>() { "unittest" },
                });
            }
        }

        /// <summary>
        /// CreateExistingProduct
        /// </summary>
        [Test]
        public void CreateExistingProduct()
        {
            foreach (var configfile in Common.configurations)
            {
                Namespace.db = new Database(configfile);
                var product = new Monosoft.Service.Product.V1.DTO.Product()
                {
                    id = Guid.NewGuid(),
                    json = jsontext,
                    categories = new List<string>() { "unittest" },
                };

                create cmd = new create();
                cmd.Execute(product);
                Assert.Throws<ITUtil.Common.Base.ElementAlreadyExistsException>(() => cmd.Execute(product));
            }
        }

        /// <summary>
        /// UpdateProduct
        /// </summary>
        [Test]
        public void UpdateProduct()
        {
            foreach (var configfile in Common.configurations)
            {
                Namespace.db = new Database(configfile);
                var productId = Guid.NewGuid();
                var product =  new Monosoft.Service.Product.V1.DTO.Product()
                    {
                        id = productId,
                        json = jsontext,
                        categories = new List<string>() { "unittest" },
                    };
                create cmd = new create();
                cmd.Execute(product);


                var updateproduct = new Monosoft.Service.Product.V1.DTO.Product()
                    {
                        id = productId,
                        json = jsontext,
                        categories = new List<string>() { "unittest update" },
                    };

                update updatecmd = new update();
                updatecmd.Execute(updateproduct);
            }
        }

        /// <summary>
        /// UpdateProduct
        /// </summary>
        [Test]
        public void UpdateNonExistingProduct()
        {
            foreach (var configfile in Common.configurations)
            {
                Namespace.db = new Database(configfile);
                var productId = Guid.NewGuid();

                var product = new Monosoft.Service.Product.V1.DTO.Product()
                    {
                        id = productId,
                        json = jsontext,
                        categories = new List<string>() { "unittest update" },
                    };

                update updatecmd = new update();
                Assert.Throws<ITUtil.Common.Base.ElementDoesNotExistsException>(() =>
                    updatecmd.Execute(product)
                );
            }
        }

        /// <summary>
        /// ReadProduct
        /// </summary>
        [Test]
        public void ReadProduct()
        {
            foreach (var configfile in Common.configurations)
            {
                Namespace.db = new Database(configfile);
                var productId = Guid.NewGuid();
                var json = jsontext;
                var categories = "unittest";
                var product =
                    new Monosoft.Service.Product.V1.DTO.Product()
                    {
                        id = productId,
                        json = json,
                        categories = new List<string>() { categories },
                    };
                create cmd = new create();
                cmd.Execute(product);

                read readcmd = new read();
                var res = readcmd.Execute(new Monosoft.Service.Product.V1.DTO.GuidIdDTO() { id = productId });

                Assert.AreEqual(res.id, productId);
                Assert.AreEqual(res.json, json);
                Assert.AreEqual(res.categories[0], categories);
            }
        }

        ///// <summary>
        ///// ReadProduct
        ///// </summary>
        // [Test]
        // public void ReadAll()
        // {
        //    foreach (var configfile in Common.configurations)
        //    {
        //        Controller.db = new Database(configfile);
        //        var wrapper = Common.GetMockWrapper();
        //        ITUtil.Common.DTO.MessageWrapperHelper<Monosoft.Service.Product.V1.DTO.ExcludepProperty>.SetData(
        //        wrapper,
        //        new Monosoft.Service.Product.V1.DTO.ExcludepProperty()
        //        {
        //            excludeProperty = new List<string>() { "variants.name", "variants.pages" },
        //        });
        //        var res = Controller.GetAll(wrapper);
        //        var input = MessageDataHelper.FromMessageData<List<Monosoft.Service.Product.V1.DTO.Product>>(res.data);
        //        for (int i = 0; i < input.Count; i++)
        //        {
        //            Console.WriteLine(input[i].json);
        //        }
        //    }
        // }
    }
}
