﻿using ITUtil.Common.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Unittests
{
    public class Common
    {
        public static string[] configurations = { "appsettings.mssql.json", "appsettings.postgresql.json" };

        public static ITUtil.Common.DTO.MessageWrapper GetMockWrapper()
        {
            return new ITUtil.Common.DTO.MessageWrapper()
            {
                callerIp = "0.0.0.0",
                clientId = "NA",
                isDirectLink = false,
                issuedDate = DateTime.Now,
                messageId = "NA",
                tracing = new Tracing() { traceLevel = Tracing.Level.none },
                version = new ProgramVersion("1.0.0.0."),
            };
        }
    }
}